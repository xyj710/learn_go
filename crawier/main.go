package main

import (
	"bufio"
	_ "encoding"
	"fmt"
	"golang.org/x/net/html/charset"
	"golang.org/x/text/encoding"
	_ "golang.org/x/text/encoding/simplifiedchinese"
	"io"
	"io/ioutil"
	"net/http"
	"regexp"
)

func main() {
	resp, err := http.Get("http://www.zhenai.com/zhenghun")
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Println("Error : status code", resp.StatusCode)
		return
	}
	/*
		e := determineEncoding(resp.Body)

		//utf8Reader := transform.NewReader(resp.Body, simplifiedchinese.GBK.NewDecoder())
		utf8Reader := transform.NewReader(resp.Body, e.NewDecoder())
		all, err := ioutil.ReadAll(utf8Reader)
	*/
	all, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	printCityList(all)
	//fmt.Printf("%s\n", all)
}

func determineEncoding(r io.Reader) encoding.Encoding {
	bytes, err := bufio.NewReader(r).Peek(1024)
	if err != nil {
		panic(err)
	}
	e, _, _ := charset.DetermineEncoding(bytes, "")

	return e
}

func printCityList(contents []byte) {
	re := regexp.MustCompile(`<a href="(http://www.zhenai.com/zhenghun/[0-9a-z]+)"[^>]*>([^<]+)</a>`)
	/*matches := re.FindAll(contents, -1)

	for _, m := range matches {
		fmt.Printf("%s\n", m)
	}
	fmt.Println(len(matches))
	*/
	matches := re.FindAllSubmatch(contents, -1)
	/*for _, m := range matches {
		for _, subMatch := range m {
			fmt.Printf("%s ", subMatch)
		}
		fmt.Println()
	}*/
	for _, m := range matches {
		fmt.Printf("city:%s url:%s\n", m[2], m[1])
	}
}
