package main

import (
	"fmt"
	"runtime"
	"time"
)

func main() {
	/*for i := 0; i < 1000; i++ {
		go func(i int) {
			for {
				fmt.Printf("Hello World from goroutine %d\n", i)
			}
		}(i)
	}
	time.Sleep(10*time.Microsecond)
	*/

	var a [10]int
	for i := 0; i < 10; i++ {
		//go func() {
		go func(i int) {
			for {
				a[i]++
				runtime.Gosched()
			}
		}(i)
		//}()
	}
	time.Sleep(time.Microsecond)
	fmt.Println(a)
	//time.Sleep(time.Second)
}
