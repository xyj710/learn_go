package main

import (
	"fmt"
	"sync"
)

type worker struct {
	in   chan int
	done func()
}

func createWorker(id int, wg *sync.WaitGroup) worker {
	w := worker{
		in: make(chan int),
		done: func() {
			wg.Done()
		},
	}
	//tcp.Conn
	go doWorker(id, w)
	return w
}

func doWorker(id int, w worker) {
	for n := range w.in {
		fmt.Printf("Worker %d received %c\n", id, n)
		w.done()

	}
}

func chanDemo() {
	var wg sync.WaitGroup
	var workers [10]worker
	for i := 0; i < 10; i++ {
		workers[i] = createWorker(i, &wg)
	}
	wg.Add(30)
	for i, w := range workers {
		w.in <- 'a' + i
	}

	for i, w := range workers {
		w.in <- 'A' + i
	}

	for i, w := range workers {
		w.in <- '0' + i
	}

	wg.Wait()
	//time.Sleep(time.Microsecond)
}

func main() {
	fmt.Println("Channel as first-class citizen")
	chanDemo()
}
