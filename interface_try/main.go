package main

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

func printFileContents(reader io.Reader) {
	scanner := bufio.NewScanner(reader)

	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func main() {
	s := `123"a
	b	
	good--我是罗杰\n
	"age=33
	罗明灿`

	printFileContents(strings.NewReader(s))
}
