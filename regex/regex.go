package main

import (
	"fmt"
	"regexp"
)

const text = `my email is luojie@163.com@abc.com
email2 is 60249999@qq.com
email3 is luojie@gmail.com
email4 is luojie@abc.de.com`

func main() {
	re := regexp.MustCompile(`([0-9a-zA-Z]+)@([0-9a-zA-Z]+)(\.[a-zA-Z0-9.]+)`)
	//re := regexp.MustCompile(`.+@.+\\..+`)
	//match := re.FindString(text)
	//match := re.FindAllString(text, -1)
	//fmt.Println(match)

	match := re.FindAllStringSubmatch(text, -1)
	for _, m := range match {
		fmt.Println(m)
	}

}
