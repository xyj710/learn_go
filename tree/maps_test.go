package main

import "testing"

func maxSubStringLength(s string) int {
	//occured := make(map[byte]int)
	occured := make(map[rune]int)
	start := 0
	maxLength := 0
	for i, ch := range []rune(s) {
		if chI, ok := occured[ch]; ok {
			if chI >= start {
				start = occured[ch] + 1
			}
		}

		if i - start + 1 > maxLength {
			maxLength = i - start + 1
		}

		occured[ch] = i
	}
	return maxLength
}

func TestSubStr(t *testing.T)  {
	tests := []struct{
		s string
		ans int
	} {
		{ "abcabcabc", 3},
		{"pwwkew", 3},

		{"b", 1},
		{"bbbbbbbbbbbbbb", 1},

		{"我爱刘婷和罗明灿", 8},
		{"一二三二一", 3},

	}

	for _, tt := range tests {
		if actual := maxSubStringLength(tt.s); actual != tt.ans {
			t.Errorf("maxSubStringLength(%s) expect %d but value is %d",
				tt.s, tt.ans, actual)
		}
	}
	
}
