package main

import (
	"fmt"
	"tree/node"
)

func main() {
	var root tree.Node
	//fmt.Println(root)

	root = tree.Node{Value: 3}
	root.Left = &tree.Node{}
	root.Right = &tree.Node{5, nil, nil}

	root.Right.Left = new(tree.Node)
	root.Left.Right = tree.CreateNode(2)

	root.Right.Left.SetValue(4)

	/*
		fmt.Println(root)

		root.Print()
		fmt.Println()

		root.Right.Left.SetValue(4)
		root.Right.Left.Print()
		fmt.Println()


		root.SetValue(100)
		root.Print()
		fmt.Println()

		var pRoot *tree.Node
		pRoot.SetValue(200)
		pRoot = &tree.Node{Value:  88,}
		pRoot.Print()
	*/

	root.Traverse()

	nodeCount := 0
	root.TraverseFunc(func(node *tree.Node) {
		nodeCount++
	})

	fmt.Println("Node count:", nodeCount)
}
