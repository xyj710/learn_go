package tree 

import (
	"fmt"

)

type Node struct {
	Value int
	Left, Right *Node
}

func (node *Node) Print() {
	fmt.Print(node.Value, " ")
}

func (node *Node) SetValue(value int) {
	if node == nil {
		fmt.Println("setting value to nil node. Ignored")
		return
	}

	node.Value = value
}

func (node *Node) Traverse() {
	node.TraverseFunc(func(n *Node) {
		n.Print()
	})
	fmt.Println()
}

func CreateNode(value int) *Node {
	//返回了局部变量，但是go依然是正常的
	return &Node{Value:value}
}

func (node *Node) TraverseFunc(
	f func(*Node)) {
		if node == nil {
			return
		}

		node.Left.TraverseFunc(f)
		f(node)
		node.Right.TraverseFunc(f)

}


//nil可以调用方法

//值接收者 vs 指针接收者
//1. 要改变内容必须使用指针接收者
//2. 结构过大也考虑使用指针接收者
//3. 一致性：如有指针接收者，最好都是指针接收者

//.....
//值接收者是go语言特有
//值/指针接收者均可接收值/r指针


