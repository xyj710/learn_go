package main

import (
	"../../rpc"
	"log"
	"net"
	"net/rpc/jsonrpc"
)

func main() {
	conn, err := net.Dial("tcp", ":1234")
	if err != nil {
		panic(conn)
	}

	client := jsonrpc.NewClient(conn)
	var result float64
	err = client.Call("DemoService.Div", rpcDemo.Args{10, 3}, &result)
	if err != nil {
		log.Println(err)
	} else {
		log.Println(result)
	}

	err = client.Call("DemoService.Div", rpcDemo.Args{10, 0}, &result)
	if err != nil {
		log.Println(err)
	} else {
		log.Println(result)
	}
}
