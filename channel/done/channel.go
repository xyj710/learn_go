package done

import (
	"fmt"
	"time"
)

func createWorker(id int) chan<- int {
	c := make(chan int)
	go worker(id, c)
	return c
}

/*
func worker(id int, c chan int) {
	for {
		n := <- c
		fmt.Printf("Worker %d received %c\n",
			id, n)
	}
}*/

func worker(id int, c chan int) {
	for n := range c {
		//第一种做法
		/*n, ok := <- c
		if !ok {
			break
		}*/
		fmt.Printf("Worker %d received %c\n",
			id, n)
	}
}

func bufferedChannel() {
	c := make(chan int, 3)
	go worker(0, c)
	c <- 'a'
	c <- 'b'
	c <- 'c'
	c <- 'd'
	time.Sleep(time.Microsecond)
}

func channelClose() {
	c := make(chan int)
	go worker(0, c)
	c <- 'a'
	c <- 'b'
	c <- 'c'
	c <- 'd'
	close(c)
	time.Sleep(time.Microsecond)

}

func chanDemo() {
	//c := make(chan int)
	//go worker(0, c)

	var channels [10]chan<- int
	for i := 0; i < 10; i++ {
		channels[i] = createWorker(i)
		//已经将channel定义为单向
		//n := <-channels[i]
	}

	/*for i := 0; i < 10; i++ {
		channels[i] = make(chan int)
		go worker(i, channels[i])
	}*/

	for i := 0; i < 10; i++ {
		channels[i] <- 'a' + i
	}

	for i := 0; i < 10; i++ {
		channels[i] <- 'A' + i
	}

	for i := 0; i < 10; i++ {
		channels[i] <- 'a' + i
	}

	for i := 0; i < 10; i++ {
		channels[i] <- '0' + i
	}
	/*go func() {
		for {
			n := <- c
			fmt.Println(n)
		}
	}()*/

	//c <- 1
	//c <- 2
	time.Sleep(time.Microsecond)

}

func main() {
	fmt.Println("Channel as first-class citizen")
	chanDemo()
	fmt.Println("Buffered channel")
	bufferedChannel()
	fmt.Println("Channel close")
	channelClose()
}
