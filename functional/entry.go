package main

import (
	"bufio"
	"fmt"
	"io"
	"strings"
	"functional/fibonacci"
)

type intGen func() int

//Read 接口需要对于p的大小来判定，以符合标准的read接口
func (g intGen) Read(p []byte) (n int, err error) {
	next := g()
	if next > 1000000 {
		return 0, io.EOF
	}
	s := fmt.Sprintf("%d\n", next)
	return strings.NewReader(s).Read(p)
}

func printFileContents(reader io.Reader) {
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func main() {
	var f intGen = fib.Fibonacci()
	printFileContents(f)
	/*fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())*/


	f() //1
	f() //1
	f() //2

}
