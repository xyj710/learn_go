package main

import (
	"fmt"
	"retriever/mock"
	"retriever/real"
	"time"
)

type Retriever interface {
	Get(url string) string
}

type Poster interface {
	Post(url string, form map[string]string) string
}

func download(r Retriever) string {
	return r.Get("http://www.baidu.com")
}

func post(poster Poster) {
	poster.Post("http://www.imooc.com",
		map[string]string {
			"name": "ccmouse",
			"course": "golang",
		})
}

type RetrieverPoster interface {
	Retriever
	Poster
}

func session(s RetrieverPoster) {
	s.Get()
	s.Post()
}


func inspect(r Retriever) {
	fmt.Printf("%T %v\n", r, r)
	switch v := r.(type) {
	case mock.Retriever:
		fmt.Println("Contents:", v.Contents)
	case *real.Retriever:
		fmt.Println("UserAgent:", v.UserAgent)
	}
}
func main() {
	var r Retriever
	r = mock.Retriever{"this is a fake baidu"}
	fmt.Println(download(r))
	//fmt.Printf("%T %v\n", r, r)
	inspect(r)
	r = &real.Retriever{
		UserAgent:"Mozilla/5.0",
		Timeout: time.Minute,
	}
	//fmt.Printf("%T %v\n", r, r)
	inspect(r)

	//fmt.Println(download(r))
	realRetriever := r.(*real.Retriever)
	fmt.Println(realRetriever.Timeout)
}
