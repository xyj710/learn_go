package main

import (
	"fmt"
	"strconv"
)

func array(arr [5]int) {
	for _, v := range arr {
		fmt.Println(v)
	}
}

func main () {

	var arr1 [5]int
	//arr2 := [...]int{1, 2, 3}
	arr3 := [...]int{2, 4, 6, 8, 10}
	array(arr1)
	//array(arr2)
	array(arr3)

	a := 3
	fmt.Printf("%s", strconv.Itoa(a))
}

