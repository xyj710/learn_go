package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	s := "Yes我爱刘婷!" //UTF-8编码 可变长

	fmt.Println(len(s))
	fmt.Printf("%X\n", []byte(s))

	for _, b := range []byte(s) {
		fmt.Printf("%x ", b)
	}
	fmt.Println()

	for i, ch := range s {
		fmt.Printf("(%d %x) ", i, ch)
	}
	fmt.Println()

	fmt.Println("Rune count:",
		utf8.RuneCountInString(s))

	bytes := []byte(s)
	for len(bytes) > 0 {
		ch, size := utf8.DecodeRune(bytes)
		bytes = bytes[size:]
		fmt.Printf("%c ", ch)
	}
	fmt.Println()
	//下面的强制类型转换 已经变成了不同的底层数组
	for i, ch := range []rune(s) {
		fmt.Printf("(%d %c) ", i, ch)
	}
	fmt.Println()

}
//
//rune 相当于go的char
//使用range遍历pos，rune对
//使用utf8.RuneCountString获得字符数量
//使用len获得字节长度
//使用[]byte获得字节