package main

import "fmt"

// abcabcbb

func maxSubStringLength(s string) int {
	//occured := make(map[byte]int)
	occured := make(map[rune]int)
	start := 0
	maxLength := 0
	for i, ch := range []rune(s) {
		if chI, ok := occured[ch]; ok {
			if chI >= start {
				start = occured[ch] + 1
			}
		}

		if i - start + 1 > maxLength {
			maxLength = i - start + 1
		}

		occured[ch] = i
	}
	return maxLength
}

func main() {
	fmt.Println(maxSubStringLength("abcabcbb"))
	fmt.Println(maxSubStringLength("bbbbbbb"))
	fmt.Println(maxSubStringLength("abcdefgh"))
	fmt.Println(maxSubStringLength("我爱刘婷和罗明灿"))
	fmt.Println(maxSubStringLength("一二三二一"))
}
